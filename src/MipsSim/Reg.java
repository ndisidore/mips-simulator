package MipsSim;

public class Reg {

    // Register Pieces
    public Integer  Number;
    public Integer  Value;
    public boolean  Ready;

    public Reg(int num, int val) {
        this.Number = num;
        this.Value  = val;
        this.Ready  = true;
    }
    
    public Reg() {
        this.Number = -1;
        this.Value  = 0;
        this.Ready  = true;
    }
    
    public int checkOutReg() {
        /**
         * Reads the value currently in the register and also marks
         * the register as busy until the register is checked back in
         * 
         * @return  the value currently in the register
         */
        //When a register is checked out, it won't be ready until
        // it gets checked back in
        this.Ready = false;
        return readReg();
    }
    
    public boolean checkInReg(int newVal) {
        /**
         * Releases a register back to the system, updating it's value
         * and marking it as ready for subsequent instructions
         * 
         * @param   newVal  the updated register value
         */
        this.Ready  = true;
        return setValue(newVal);
    }
    
    public int readReg() {
        /**
         * Reads the register value without changing the ready bit
         *  
         *  @return the value currently in the register
         */
        return this.Value;
    }
    
    public boolean setValue(int newVal) {
        /**
         * Checks if the Reg is ready, and if so, assigns it a new value
         * 
         * @param   val new value of register
         */
        if (!this.Ready) {
            SimUtils.display("DEBUG: Trying to set value for register " + this.Number + " before ready.");
            return false;
        }
        this.Value = newVal;
        return true;
    }

    
    // READY Functions
    public boolean isReady() {
        return this.Ready;
    }

    public void setReady(boolean r) {
        this.Ready = r;
    }

    public void flipReady() {
        this.Ready = !this.Ready;
    }

}
