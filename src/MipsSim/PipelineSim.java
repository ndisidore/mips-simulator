package MipsSim;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

import MipsSim.Instruction.InstName;
import MipsSim.Instruction.InstState;
import MipsSim.Instruction.StageName;

public class PipelineSim {
	
    //HashMap representing the memory state. Stored as <Location, Value>
    public static HashMap<Integer, Integer> memory = new HashMap<Integer, Integer>();
    //HashMap that holds the branch locations in memory. Stored as <BranchID, Location>
    public static HashMap<String, Integer> branchLocations = new HashMap<String, Integer>();
    //Array List of all instructions 
    public static ArrayList<String> instList = new ArrayList<String>();
    public static LinkedList<PipelineLatch> pipeLatches = new LinkedList<PipelineLatch>();
    public static Reg[]     Registers;      //Array of registers [R0...R31]
    public static int       numInst;        //Number of processed instructions
    public static boolean   isStall;        //Is the pipeline stalled?
    public static boolean   isFlush;        //Missed branch prediction => flush
    public static int       PC;             //Program Counter of the Simulator
    public static int       clockCycle;     //Current Clock Cycle
    public static String    inFile;         //Input File
    public static String    outFile;        //Output File

    
    public static void main(String[] args) {
        String runAgain = null;
        do {
            BufferedReader prompt = new BufferedReader(new InputStreamReader(System.in));
            SimUtils.display("MIPS 8-STAGE PIPELINE SIMULATOR");
            SimUtils.display("Configuration:");
            try {
                SimUtils.display(" Input File: ", false);
                inFile = prompt.readLine();
                SimUtils.display(" Output File: ", false);
                outFile = prompt.readLine();
            } catch (IOException e) {
                SimUtils.display("There was an error reading your file inputs!");
                e.printStackTrace();
            }
            //inFile = "/home/dizzy/code/mips-simulator/project_input.txt";
            PipelineSim sim = new PipelineSim();
            sim.runSim();
            SimUtils.writeOutFile();
            SimUtils.display("SIMULATION COMPLETE");
            SimUtils.display("Run another simulation? (y/n): ", false);
            try {
                runAgain = prompt.readLine();
            } catch(IOException ex) {
                SimUtils.display("ERROR: There was an error reading your choice.");
                ex.printStackTrace();
            } 
        } while (runAgain.toLowerCase().equals("y"));
        SimUtils.display("Simulator Exiting");
    }
    
    public void runSim() {
        /**
         * Executes the pipeline simulation
         */
        //Reset all our local variables
        clockCycle = 1;
        PC = 0;
        numInst = 1;
        memory.clear();
        branchLocations.clear();
        instList.clear();
        pipeLatches.clear();
        Registers = new Reg[32];
        isStall = false;
        isFlush = false;
        SimUtils.outFileText.setLength(0);  //Flush the StringBuffer output text
        
        //Now need to parse the input file
        @SuppressWarnings("unused")
        InputParser ip = new InputParser(inFile);
        
        while (canRun()) {
            SimUtils.toOutfile("CC#" + clockCycle, false);
            //As mentioned in class, we need to do this in reverse order
            // so that the latches don't get overwritten until AFTER we're done
            // using them
            WB();           //Write Back
            MEM3();         //Memory 3
            MEM2();         //Memory 2
            MEM1();         //Memory 1
            EX();           //Execution
            ID();           //Instruction Decode
            IF2();          //Instruction Fetch 2
            IF1();          //Instruction Fetch 1
            clockCycle++;
            SimUtils.toOutfile("");
        }
        SimUtils.printPipelineResults();
    }

    public boolean canRun() {
        /**
         * Makes sure the pipeline isn't empty or full
         */
        //Make sure not full
        if (pipeLatches.size() < 7)
            return true;
        
        //Checks to make sure the latch list isn't empty
        for (PipelineLatch pl : pipeLatches) {
            if (pl != null)
                return true;
        }
        
        return false;
    }

    public void IF1() {
        /**
         * Instruction Fetch Stage 1
         */
        if((PipelineSim.PC < instList.size()) && !isStall) {
            //Fetch instruction from memory
            //Initialize the instruction object with the next instruction
            String rawInst = PipelineSim.instList.get(PC);    //Get the instruction at the PC
            Instruction Inst = new Instruction(rawInst);        //Create a new in
            PipelineLatch if1 = new PipelineLatch(Inst);        //Create a new pipeline latch    
            Inst.atStage = StageName.IF1;                       //Set stage to IF1
            pipeLatches.addFirst(if1);                          //Add it to the beginning of the latch set
            if1.Inst.printCurrentState();                       //Print the current state
            PipelineSim.PC = PipelineSim.PC + 1;                //Set the next PC
        } else if (!isStall) {
            pipeLatches.addFirst(null);
        }
    }
    
    public void IF2() {
        /**
         * Instruction Fetch Stage 2
         *  No-Op
         */
        PipelineLatch if2 = SimUtils.getLatchAt(2);
        if (if2 != null) {
            if(!isStall) {
                if2.Inst.toNextStage();
                if2.Inst.printCurrentState();
            } else {
                SimUtils.displayStall(if2.Inst.instNum);
            }
        }
    }

    public void ID() {
        /**
         * Instruction Decode
         * Turns the raw instruction text into usable data
         */
        PipelineLatch id = SimUtils.getLatchAt(3);
        if (id != null) {
            if(!isStall) {
                id.Inst.decode();               //Run the instruction's decode method
                id.Inst.toNextStage();
                id.Inst.printCurrentState();
            } else {
                SimUtils.displayStall(id.Inst.instNum);
            }
        }
    }

    public void EX() {
        /**
         * Execution
         * Performs the operation or function specified on the given
         * operands
         */
        PipelineLatch ex = SimUtils.getLatchAt(4);
        if (ex != null) {
            PipelineSim.isStall = ex.stallNeeded();
            if (!isStall) {
                if (ex.Inst.control.ALUOp) {
                    int result=0, tmpA=0, tmpB=0;
                    //It's an ALU operation (specifically SUB or DADD)
                    tmpA = ex.Inst.src1.readOperandValue();
                    tmpB = ex.Inst.src2.readOperandValue();
                    if (ex.Inst.name == InstName.SUB)
                        result = tmpA - tmpB;
                    else if (ex.Inst.name == InstName.DADD)
                        result = tmpA + tmpB;
                    ex.ALUOutput.setValue(result);
                } else if (ex.Inst.name == InstName.BNEZ) {
                    // It's a BNEZ instruction, they do things differently
                    int src1Val = ex.Inst.src1.readOperandValue();
                    boolean tmpBool = (src1Val == 0) ? false : true;
                    ex.Cond = new Boolean(tmpBool);
                } else if (ex.Inst.name == InstName.LD) {
                    ex.ALUOutput.setValue(ex.Inst.src1.readOperandValue());
                } else if (ex.Inst.name == InstName.SD) {
                    ex.ALUOutput.setValue(ex.Inst.dest.readOperandValue());
                }
                ex.Inst.toNextStage();
                ex.Inst.printCurrentState();
            } else {
                //This is a special case for the stall. It was added here
                SimUtils.displayStall(ex.Inst.instNum);
                pipeLatches.add(3,null);
            }
        }
    }

    public void MEM1() {
        /**
         * Memory Access/Branch Completion Stage 1
         * Access Main Memory if needed
         */
        PipelineLatch mem1 = SimUtils.getLatchAt(5);
        if (mem1 != null) {
            //The PC is updated for all instructions
            //PipelineSim.PC = mem1.NPC;                    //PC <- NPC
            if (mem1.Inst.name == InstName.LD) {
                //If load, data returns from memory and is placed in the LMD
                mem1.LMD.setValue(SimUtils.memoryAt(mem1.ALUOutput.readReg()));   //LMD <- Mem[ALUOutput]
            } else if (mem1.Inst.name == InstName.SD) {
                //If store, data from B reg is written into memory - Mem[ALUOutput] <- B
                SimUtils.setMemoryAt(mem1.ALUOutput.readReg(), mem1.Inst.src1.readOperandValue());
            } else if (mem1.Inst.name == InstName.BNEZ) {
                //Special logic for branch not equal
                if (mem1.Cond == true) {
                    //flush the current pipeline IF2 and ID Stage
                    SimUtils.flushLine();
                    //update the PC
                    PipelineSim.PC = branchLocations.get(mem1.Inst.branchTarget);
                }
            }
            mem1.Inst.toNextStage();
            mem1.Inst.printCurrentState();
        }
    }
    
    public void MEM2() {
        /**
         * Memory Access/Branch Completion Stage 2
         * No-op
         */
        PipelineLatch mem2 = SimUtils.getLatchAt(6);
        if (mem2 != null) {
            mem2.Inst.toNextStage();
            mem2.Inst.printCurrentState();
        }
    }
    
    public void MEM3() {
        /**
         * Memory Access/Branch Completion Stage 3
         * No-op
         */
        PipelineLatch mem3 = SimUtils.getLatchAt(7);
        if (mem3 != null) {
            mem3.Inst.toNextStage();
            mem3.Inst.printCurrentState();
        }
    }

    public void WB() {
        /**
         * Write Back
         * Write result into the destination register file
         */
        PipelineLatch wb = SimUtils.getLatchAt(8);
        int val = 0;
        if (wb != null) {
            
            //Only need to write back depending on whether the regWrite
            //control flag was set
            if (wb.Inst.control.RegWrite) {
                //Loads write to the register from LMD while ALU
                //instructions write from the ALUOutput
                if (wb.Inst.control.MemToReg)
                    val = wb.LMD.readReg();
                else
                    val = wb.ALUOutput.readReg();
                //Finally write the result to the regfile
                wb.Inst.writeInstResult(val);
            }
            
            wb.Inst.toNextStage();
            wb.Inst.printCurrentState();
            wb.Inst.state = InstState.DONE;
            
            pipeLatches.removeLast();
        } else if (pipeLatches.size() >= 7) {
            pipeLatches.removeLast();
        }
    }
}
