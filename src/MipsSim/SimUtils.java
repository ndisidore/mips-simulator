package MipsSim;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

public class SimUtils {
    
    public static StringBuffer outFileText = new StringBuffer();

    public static void error(String e) {
        /**
         * Helper method to pretty-fy what's displayed
         * 
         * @param   e       error message
         */
        error(e, false);
    }
    
    public static void error(String e, boolean fatal) {
        /**
         * Helper method to pretty-fy what's displayed
         * 
         * @param   e       error message
         * @param   fatal   boolean specifying whether fatal
         */
        if (fatal == true) {
            display(e);
            System.exit(1);
        } else {
            display(e);
        }
    }
    
    public static void toOutfile(String s) {
        /**
         * Helper method to pretty-fy what's displayed
         * 
         * @param   out string to print
         */
        toOutfile(s, true);
    }
    
    public static void toOutfile(String s, boolean nl) {
        /**
         * Helper method to pretty-fy what's displayed
         * 
         * @param   out string to print
         * @param   nl  boolean specifying whether to use a new line
         */
        try {
            if (nl)
                outFileText.append(s + "\n");
            else
                outFileText.append(s);
        } catch (java.lang.OutOfMemoryError e) {
            SimUtils.error("ERROR: The JVM ran out of memory trying to append to the output.\n    Make sure there's not an infinite loop with branches!", true);
        }
    }
    
    public static void display(String out) {
        /**
         * Helper method to pretty-fy what's displayed
         * 
         * @param   out string to print
         */
    	display(out, true);
    }
    
    public static void display(String out, boolean nl) {
        /**
         * Helper method to pretty-fy what's displayed
         * 
         * @param   out string to print
         * @param   nl  boolean specifying whether to use a new line
         */
    	if(nl == true)
    		System.out.println(out);
    	else
    		System.out.print(out);
    }
    
    public static void displayStall(int instNum) {
        /**
         * Standard stall display function
         * 
         * @param   instNum instruction number that stalled
         */
        SimUtils.toOutfile(" I" + instNum + "-stall", false);
    }
    
    public static void printPipelineResults() {
        /**
         * Adds contents of the registers and main memory to
         * the output file text
         */
        //Print the contents of the registers
        toOutfile("REGISTERS");
        for (Reg r : PipelineSim.Registers) {
            if (r != null && r.Value != 0)
                toOutfile("R" + r.Number + " " + r.Value);
        }
        //Print the contents of main memory
        toOutfile("MEMORY");
        //Sort the HashMap with a temporary TreeMap
        Map<Integer, Integer> memMap = new TreeMap<Integer, Integer>(PipelineSim.memory);
        for (Integer loc : memMap.keySet()) {
            toOutfile(loc + " " + memMap.get(loc));
        }
    }
    
    public static boolean setRegValue(String reg, int newVal) {
        /**
         * Attempts to set the specified register with the value
         * in newVal
         * 
         * @param   reg     string representation of the register
         * @param   newVal  the desired new value for the register
         */
        String strVal = reg.substring(1, reg.length()-1);
        int regNum = Integer.parseInt(strVal);
        return setRegValue(regNum, newVal);
    }
    
    public static boolean setRegValue(int regNum, int newVal) {
        /**
         * Attempts to set the specified register with the value
         * in newVal
         * 
         * @param   reg     integer index of the register
         * @param   newVal  the desired new value for the register
         */
        if (regNum < 0 || regNum > 31) {
            SimUtils.error("ERROR: Trying to set the value of a register that falls out of bounds: " + regNum);
            return false;
        }
        
        //If the register hasn't been set up yet, then do so
        if (PipelineSim.Registers[regNum] == null) {
            Reg thisReg = new Reg(regNum, newVal);
            PipelineSim.Registers[regNum] = thisReg;
            return true;
        }
        
        //Otherwise just get it from the RegFile
        Reg theReg = PipelineSim.Registers[regNum];
        return theReg.setValue(newVal);
    }
    
    public static Integer getRegValue(String reg) {
        /** 
         * Gets the value stored in the specified register
         * 
         * @param   reg Register in question in RX+ format where X = reg. number
         * @return      the value stored in the register
         */
        //First turn the RXX string into an integer for index in reg array
        String strVal = reg.substring(1, reg.length()-1);
        int regNum = Integer.parseInt(strVal);
        
        //Else get the value
        return getRegValue(regNum);
    }
    
    public static Integer getRegValue(int reg) {
        /** 
         * Gets the value stored in the specified register
         * 
         * This is the overloaded integer version
         * 
         * @param   reg Register in question
         * @return      the value stored in the register
         */
        Reg wantedReg;
        
        //First some basic error checking
        if (reg < 0 || reg > 31)
            error("ERROR: Attempting to get the value of a register that falls out of bounds:" + reg);
        
        //If the register doesn't have a value, assume 0
        if (PipelineSim.Registers[reg] == null || reg == 0)
            return 0;
        
        wantedReg = PipelineSim.Registers[reg];
        //If the register is not ready (i.e. a hazard like RAW), need to signal
        // the caller. Do this by returning null.
        if (!wantedReg.isReady())
            return null;
        
        return wantedReg.readReg();
    }
    
    public static Integer memoryAt(int location) {
        /**
         * memoryAt - get value at memory value at location
         *
         * @param   location    memory location to retrieve from
         */
        //Need to do a check to make sure the location is a multiple of 8
        // and also less than the max memory location of 992
        if(location % 8 != 0) {
            //ERROR: Memory location given is not a multiple of 8
            SimUtils.error("ERROR: Memory location " + location + " is not a multiple of 8");
            return null;
        } else if (location < 0 || location > 992) {
            //ERROR: Memory location given falls out of acceptable bounds
            SimUtils.error("ERROR: Memory location " + location + " falls out of acceptable bounds");
            return null;
        }
        
        //Now that the memory location is known, get the value at that
        // memory slot
        Integer memValue = PipelineSim.memory.get(location);

        if (memValue == null)
            return 0;
        else
            return memValue;
    }

    public static Integer memoryAt(String reg, int offset) {
        /**
         * memoryAt - get value at memory location specified by reg + offset
         *
         * @param   reg     register to start at
         * @param   offset  offset to add to register value
         *
         */
        int memLocation = getRegValue(reg);
        memLocation += offset;

        return memoryAt(memLocation);
    }
    
    public static Integer memoryAt(int reg, int offset) {
        /**
         * memoryAt - get value at memory location specified by reg + offset
         *
         * @param   reg     register to start at
         * @param   offset  offset to add to register value
         *
         */
        int memLocation = getRegValue(reg);
        memLocation += offset;

        return memoryAt(memLocation);
    }
    
    public static boolean setMemoryAt(int location, int val) {
        /**
         * Sets the main memory location to the value specified
         * 
         * @param   location    address in memory
         * @param   val         new value
         * @return  boolean indicating success or failure
         */
        
        //Check error conditions
        if(location % 8 != 0) {
            //ERROR: Memory location given is not a multiple of 8
            SimUtils.error("ERROR: Memory location " + location + " is not a multiple of 8");
            return false;
        } else if (location < 0 || location > 992) {
            //ERROR: Memory location given falls out of acceptable bounds
            SimUtils.error("ERROR: Memory location " + location + " falls out of acceptable bounds");
            return false;
        }
        
        //If no error, try an add it
        try {
            PipelineSim.memory.put(location, val);
        } catch (Exception e) {
            SimUtils.error("ERROR: Could not add the value " + val + " to memory location " + location);
            return false;
        }
        return true;
    }
    
    public static PipelineLatch getLatchAt(int i) {
        /**
         * Returns the Pipeline latch for position i
         * 
         * @param   i   latch location in pipeline
         * @return PipelineLatch at location or null if DNE
         */
        int latchListSize = PipelineSim.pipeLatches.size();
        if (latchListSize >= i-1)
            return PipelineSim.pipeLatches.get(i-2);
        else
            return null;
    }
    
    public static void flushLine() {
        /**
         * Flushes extra instructions in the pipeline if
         * there was a branch misprediction 
         */
        PipelineSim.pipeLatches.set(1, null);   //Flush IF2 Stage
        PipelineSim.pipeLatches.set(2, null);   //Flush ID Stage
    }
    
    public static void writeOutFile() {
        /**
         * Writes the simulation results to the specified
         * output file
         */
        try {
            File file = new File(PipelineSim.outFile);
            
            // if file doesn't exists, then create it
            if (!file.exists()) {
                file.createNewFile();
            }
    
            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(outFileText.toString());
            bw.close();
    
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
}
