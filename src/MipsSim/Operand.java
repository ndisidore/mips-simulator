package MipsSim;

import MipsSim.Instruction.InstName;

public class Operand {

    public enum AddressMode { REGISTER, INDIRECT, DISPLACEMENT, IMMEDIATE };
    
    public AddressMode  OpMode;
    public Integer      Offset;
    public Integer      RegRef;
    public Integer      Value;
    
    
    public Operand(String op) {
        /**
        * Determines the address mode for the given operand and
        * returns the value it calls for
        *
        * @param    op  Operand to interpret   
        */
        //Temporary Variables
        String[] tokens;
        
        op = op.trim();     //Trim Whitespace
        
        //Set all the arguments to null to start
        OpMode  = null;
        Offset  = null;
        RegRef  = null;
        Value   = null;
        
        //Determine the instruction type by iteratively refining
        // the regular expression testing
        if(op.indexOf('#') != -1)
            OpMode = AddressMode.IMMEDIATE;
        else if(op.matches("\\d+\\(R\\d+\\)"))
            OpMode = AddressMode.DISPLACEMENT;
        else if(op.matches("^\\(R\\d+\\)"))
            OpMode = AddressMode.INDIRECT;
        else if(op.matches("^R\\d+"))
            OpMode = AddressMode.REGISTER;
        else
            SimUtils.display("DEBUG: Can't find addressing mode of operand " + op);
        
        //Now that the type is know, handle accordingly
        switch (OpMode) {
            case IMMEDIATE:
                //Immediate Addressing: #A
                // Need to strip pound sign and return A
                String strVal = op.substring(1);
                Value = Integer.parseInt(strVal);
                break;
            case DISPLACEMENT:
                //Displacement Addressing: B(A)
                // Need to get memory value at B+A
                tokens = op.split("[()]+");
                RegRef = regStringtoInt(tokens[1]);
                Offset = Integer.parseInt(tokens[0]);
                break;
            case INDIRECT:
                //Register Indirect Addressing: (A)
                // Need to get memory value at A
                tokens = op.split("[()]+");
                //the first item in the tokens array will be an empty string
                RegRef = regStringtoInt(tokens[1]);
                Offset = 0;
                break;
            case REGISTER:
                //Register Addressing: A
                // Need to get the value in the register itself
                RegRef = regStringtoInt(op);
                break;
            default:
                //ERROR: Unexpected input
                SimUtils.error("ERROR: Could not parse the operand " + op);
                break;
        }
    }
      
    public Integer readOperandValue() {
        /**
         * Returns the data value the operand references
         * 
         * @return  the operand's actual value
         */
        Integer retVal = null;
        
        switch (OpMode) {
            case IMMEDIATE:
                //Immediate Addressing: #A
                retVal = Value;
                break;
            case DISPLACEMENT:
                //Displacement Addressing: B(A)
                // Need to get memory value at B+A
                retVal = SimUtils.getRegValue(RegRef) + Offset;
                break;
            case INDIRECT:
                //Register Indirect Addressing: (A)
                // Need to get memory value at A
                retVal = SimUtils.getRegValue(RegRef) + 0;
                break;
            case REGISTER:
                //Register Addressing: A
                // Need to get the value in the register itself
                retVal = getRegValue();
                break;
            default:
                SimUtils.error("ERROR: Cannot determine the operand value");
                break;
        }
        
        return retVal;
    }
    
    private Integer regStringtoInt(String reg) {
        /**
         * Parses the register number from the string representation
         * 
         * @return  the register number
         */
        String strVal = reg.substring(1);
        Integer regNum = Integer.parseInt(strVal);
        
        if (regNum < 0 || regNum > 31) {
            SimUtils.error("ERROR: Trying to set the value of a register that falls out of bounds: " + regNum);
            return null;
        }
        
        return regNum;
    }
    
    private Integer getRegValue() {
        /**
         * Gets the Value for the register
         * Looks first for forwarded data and if not available
         * Will go to the register file
         * 
         * @return  the value of the referenced register
         */
        Integer retVal = null;
        //Check for forwarded data in the later registers
        for (int i = 7; i>3; i--) {
            PipelineLatch eLatch = SimUtils.getLatchAt(i);
            if (eLatch != null && eLatch.Inst.control.RegWrite 
                    && (eLatch.Inst.dest.RegRef == this.RegRef)) {
                //If alu operation available after the EX stage
                //If load available after MEM2
                if (eLatch.Inst.control.ALUOp && (i>4))
                    retVal = eLatch.ALUOutput.readReg();
                else if ((eLatch.Inst.name == InstName.LD) && (i>5))
                    retVal = eLatch.LMD.readReg();
            }
        }
        
        //If no forwarded data available, just check the 
        //regular register array 
        if (retVal == null)
            retVal = SimUtils.getRegValue(RegRef);
        
        return retVal;
    }
    
}
