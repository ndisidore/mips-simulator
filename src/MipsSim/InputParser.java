package MipsSim;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;


public class InputParser {
    //Keywords for the input file parser
    public enum Keyword { REGISTERS, MEMORY, CODE };
    public Keyword InputType;
    private int instNum = 0;
    
    public InputParser(String f) {
        readInputFile(f);
    }
    
    public void readInputFile(String fName) {
        /**
         * Reads the specified input file and calls the line parser
         * for each line
         */
        BufferedReader br = null;
 
        try {
            String sCurrentLine;
            br = new BufferedReader(new FileReader(fName));
 
            while ((sCurrentLine = br.readLine()) != null) {
                parseInput(sCurrentLine);
            }
        } catch (IOException e) {
            SimUtils.error("ERROR: There was an error reading from the specified input file");
            e.printStackTrace();
        } finally {
            try {
                if (br != null)br.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    public void parseInput(String lineIn) {
        /**
         * Handles a line from the text input file
         */
        if (!lineIn.trim().isEmpty()) {
            Keyword curLine = checkKeyword(lineIn.trim());
            
            if (curLine != null) {
                //currently read line is a keyword
                //switch the class's flag to signify the change
                InputType = curLine;
            } else if (InputType == null) {
                SimUtils.error("ERROR: Cannot find input keywords (REGISTERS/MEMORY/CODE)!", true);
            } else{
                switch (InputType) {
                    case REGISTERS:
                        parseRegs(lineIn);
                        break;
                    case MEMORY:
                        parseMemory(lineIn);
                        break;
                    case CODE:
                        parseCode(lineIn);
                        break;
                    default:
                        SimUtils.error("ERROR: Invalid input segment");
                }
            }
        }
    }

    public Keyword checkKeyword(String s) {
        /**
        * Checks to see if the incoming string is a keyword value
        *
        * @param    s   string to check against
        * @return       Keyword value if match is found, null otherwise
        */
        for(Keyword k : Keyword.values()) {
            if (k.name().equals(s)) {
                return k;
            }
        }
        return null;
    }

    public void parseRegs(String input) {
        /**
         * Load line from input file into a register
         *
         * @param   input   line from the input file
         */

        //Split incoming data on spaces to parse args
        String[] tokens = input.split("[ ]+");  //Split on 1+ spaces
        String reg = tokens[0];                 //1st Pos: Register
        String val = tokens[1];                 //2nd Pos: Value
        int regNum, regVal;
        
        //Make sure incoming register matches expected format
        if(reg.matches("^R\\d+")) {
            //Strip leading "R" and parse
            String strReg = reg.substring(1);
            regNum = Integer.parseInt(strReg);
        } else {
            //ERROR: Register not given in expected format
            SimUtils.error("ERROR: Register " + reg + " not given in expected format");
            return;
        }

        regVal = Integer.parseInt(val);

        //No errors so go ahead and put the new register data in memory
        SimUtils.setRegValue(regNum, regVal);
    }

    public void parseMemory(String input) {
        /**
         * parseMemory - load line from input file into memory
         *
         * @param   input   line from input file
         *
         */

        //Split incoming data on spaces to parse args
        String[] tokens = input.split("[ ]+");
        int location = Integer.parseInt(tokens[0]);     //1st Pos: location
        int val = Integer.parseInt(tokens[1]);          //2nd Pos: value

        //Need to do a check to make sure the location is a multiple of 8
        // and also less than the max memory location of 992
        if(location % 8 != 0) {
            //ERROR: Memory location given is not a multiple of 8
            SimUtils.error("ERROR: Memory location " + location + " is not a multiple of 8");
            return;
        } else if (location < 0 || location > 992) {
            //ERROR: Memory location given falls out of acceptable bounds
            SimUtils.error("ERROR: Memory location " + location + " falls out of acceptable bounds");
            return;
        }

        //No errors so go ahead and try to add it to the memory bank
        try {
            PipelineSim.memory.put(location, val);
        } catch (Exception e) {
            SimUtils.error("ERROR: Could not add the value " + val + " to memory location " + location);
        }

        return;
    }
    
    public void parseCode(String input) {
        /**
         * Handles the code segment of the input file
         * This is mostly just a wrapper. The heavy lifting is done
         * in the Instruction class
         * 
         * @param   input   line from input file
         */
        //First check for branch targets
        String branchName = null;
        int colonLoc = input.indexOf(":");
        if (colonLoc > -1) {
            //We've found a branch.  Go ahead and add it to the branch locations list
            branchName = input.substring(0, colonLoc).trim();
            PipelineSim.branchLocations.put(branchName, instNum);
            input = input.substring(colonLoc + 2, input.length());
        }
        instNum++;
        //Now create a new instruction and add it to the array list of 
        //instructions already in the queue
        PipelineSim.instList.add(input.trim());
    }
}
