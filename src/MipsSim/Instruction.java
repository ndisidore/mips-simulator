package MipsSim;

public class Instruction {
    public String rawText;  // The raw text string of the instruction

    // Instruction Type Enumerators
    public enum InstName        { LD, SD, DADD, SUB, BNEZ };
    public enum InstType        { MEMORY, ALU_INST, BRANCH };
    public enum InstAddressMode { REG_REG, REG_IMM };
    public enum InstState       { QUEUED, BUSY, READY, STALLED, KILLED, DONE };
    public enum StageName       { IF1, IF2, ID, EX, MEM1, MEM2, MEM3, WB };

    // Info on the instruction
    public int              instNum;    // Instruction Number
    public InstName         name;       // Name of the Instruction
    public InstType         type;       // Type of the Instruction
    public InstAddressMode  mode;       // Overall Instruction Addressing Mode
    public InstState        state;      // State of the current instruction
    public StageName        atStage;    // Stage the the instruction is current at
    public ControlBits      control = new ControlBits();    // Control bits for the Instruction

    // Info on the instruction operands
    public Operand src1; // Source Operand 1 [rs]
    public Operand src2; // Source Operand 2 [rt]
    public Operand dest; // Destination Register [rd]
    public String  branchTarget;    //Branch instruction target

    // Define a class to hold the control bits
    public class ControlBits {
        boolean RegDest;
        boolean RegWrite;
        boolean ALUSrc;
        boolean MemRead;
        boolean MemWrite;
        boolean MemToReg;
        boolean ALUOp;
        boolean Branch;
    }
    
    public Instruction(String rt) {
        //Set inst raw text as incoming string
        this.rawText = rt;
        //Set the instruction number
        this.instNum = PipelineSim.numInst;
        PipelineSim.numInst++;
    }

    public InstName checkInstruction(String s) {
        /**
         * Checks to see if the incoming string matches a known Instruction
         * 
         * @param s
         *            string to check against
         * @return Instruction name if match is found, null otherwise
         */
        for (InstName i : InstName.values()) {
            if (i.name().equals(s)) {
                return i;
            }
        }
        return null;
    }

    public void setInstAddressMode() {
        /**
         * Sets the Addressing Mode of the overall instruction based on the
         * addressing mode of the operands it contains
         * 
         * Only matters for instructions with 2 source operands (otherwise can
         * use the addressing mode of an the single operand)
         * 
         */
        // Check for 2 source operands
        if (src1.OpMode == null || src2.OpMode == null) {
            // If not 2, throw error and return
            SimUtils.error("Attempting to set the instruction addressing mode for one without two operands");
            return;
        }

        // Since one of the operands has Immediate addressing, we know the
        // instruction
        // is of register-immediate type
        if (src1.OpMode == Operand.AddressMode.IMMEDIATE
                || src2.OpMode == Operand.AddressMode.IMMEDIATE)
            mode = InstAddressMode.REG_IMM;
        else
            mode = InstAddressMode.REG_REG;
        return;
    }
    
    public void printCurrentState() {
        /**
         * Prints out the pipeline stage information for the current instruction
         * 
         */
        //If the instruction hasn't started doing it's thing yet, ignore it completely
        if (this.state == InstState.QUEUED || this.atStage == null)
            return;
        
        SimUtils.toOutfile(" I" + instNum + "-" + atStage.toString(), false);
    }

    public void printInstructionState() {
        /**
         * Prints out all the pipeline stage information for the current instruction
         * 
         */
        //If the instruction hasn't started doing it's thing yet, ignore it completely
        if (this.state == InstState.QUEUED)
            return;
        
        int stagePos = this.atStage.ordinal();
        int cnt = 0;
        StringBuilder toOut = new StringBuilder();
        while (cnt < stagePos) {
            // For each stage completed, add an X under that stage to signify completion
            toOut.append(String.format("'%7X'")); // X+6 spaces (7 chars total), right aligned
            cnt++;
        }

        // Now process the current state (don't worry about "busy" state)
        if (state == InstState.DONE || state == InstState.READY) // If completely done or ready for next stage
            toOut.append(String.format("'%7X'"));
        else if (state == InstState.STALLED)
            toOut.append(String.format("'%7S'")); // If stalled, print "S"

        SimUtils.display(toOut.toString(), false);
    }

    private void parseInst(String inst) {
        /**
         * Calls the operation based on the provided instruction type
         * 
         * @param tokens
         *            array of parsed and split vales in code segment
         */        
        inst = inst.trim();
        
        String delims   = "[ ,]+";
        String[] tokens = inst.split(delims);
        int len = tokens.length;
        name = checkInstruction(tokens[0]);
        
        //If name is null, than there's a chance the first chunk is a branch
        //target. So the second chunk should be the MIPS instruction.
        if (name == null) {
            SimUtils.error("ERROR: Cannot parse instruction " + inst);
        }
        
        //Check the number of tokens matches the expected number
        if ((name == InstName.LD || name == InstName.SD || name == InstName.BNEZ)
                && len != 3)    //3 total (Name Op1 Op2)
            SimUtils.error("ERROR: Insufficient arguments to the instruction " + name.toString());
        else if ((name == InstName.DADD || name == InstName.SUB) && len != 4)
            SimUtils.error("ERROR: Insufficient arguments to the instruction " + name.toString());

        //Handle the instruction accordingly
        switch (name) {
            case LD:
                src1 = new Operand(tokens[2]);
                src2 = null;
                dest = new Operand(tokens[1]);
                break;
            case SD:
                src1 = new Operand(tokens[1]);
                src2 = null;
                dest = new Operand(tokens[2]);
                break;
            case DADD:
                // tokens[1] = tokens[2] + tokens[3]
                src1 = new Operand(tokens[2]);
                src2 = new Operand(tokens[3]);
                dest = new Operand(tokens[1]);
                break;
            case SUB:
                // tokens[1] = tokens[2] - tokens[3]
                src1 = new Operand(tokens[2]);
                src2 = new Operand(tokens[3]);
                dest = new Operand(tokens[1]);
                break;
            case BNEZ:
                // BNEZ tokens[1], tokens[2] (R4, NEXT)
                src1 = new Operand(tokens[1]);
                branchTarget = tokens[2].trim();
                break;
            default:
                break;
        }
        
        //set the instruction's control signals
        setControlSignals();
    }
    
    public boolean writeInstResult(int val) {
        /**
         * Writes the result of the instruction (val) either from ALUOutput or
         * the LMD register to the destination register
         * 
         * @param   val the value to set the destination register
         */
        if (control.RegWrite) {
            //For ALU and Load instructions, the destination needs to be set with
            //the ALUOutput
            return SimUtils.setRegValue(dest.RegRef, val);
        } else {
            return true;
        }
    }

    private void setControlSignals() {
        // To make things easier, we'll just set them all to 0
        // first and then turn on things as necessary
        control.RegDest   = false;
        control.RegWrite  = false;
        control.ALUSrc    = false;
        control.MemWrite  = false;
        control.MemRead   = false;
        control.MemToReg  = false;
        control.Branch    = false;
        control.ALUOp     = false;
        
        switch (name) {
            case DADD:
                control.RegWrite    = true;
                control.ALUSrc      = true;
                control.ALUOp       = true;
                break;
            case SUB:
                control.RegWrite    = true;
                control.ALUSrc      = true;
                control.ALUOp       = true;
                break;
            case LD:
                control.RegDest     = true;
                control.RegWrite    = true;
                control.MemRead     = true;
                control.MemToReg    = true;
                break;
            case SD:
                control.MemWrite    = true;
                break;
            case BNEZ:
                control.RegDest     = true;
                control.Branch      = true;
                break;
            default:
                SimUtils.error("ERROR: Cannot set control signals for instruction of type " + name.toString());
                break;
        }
    }
    
    public void decode() {
        /**
         * Wrapper class for the instruction parser
         * Performs the instruction decode, setting the pieces up as necessary
         * 
         */
        if (this.rawText != null)
            parseInst(rawText);
        else
            SimUtils.error("ERROR: Cannot decode empty instruction");
    }
    
    public void toNextStage() {
        /**
         * Advances the current instruction stage to the next stage
         *  in the MIPS Pipeline
         *
         */
        if(this.state != InstState.DONE) {
            if(this.atStage != null) {
                //Make sure we're not at the end of the list
                // If we are, set the new current stage to null
                this.atStage = this.atStage.ordinal() < StageName.values().length - 1
                    ? StageName.values()[this.atStage.ordinal() + 1]
                    : null;
            } else {
                //ERROR: trying to get next stage of an unspecified latch
                SimUtils.error("There was an error trying to get next stage of instruction " + rawText);
            }
        }
    }
}
